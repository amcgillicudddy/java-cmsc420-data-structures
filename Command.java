package cmsc420.command;

import java.awt.Canvas;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeMap;
import java.util.TreeSet;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import PRQuadTree.Node;
import PRQuadTree.QuadTree;
import cmsc420.drawing.CanvasPlus;
import cmsc420.structure.City;
import cmsc420.structure.CityLocationComparator;



/**
 * Processes each command in the MeeshQuest program. Takes in an XML command
 * node, processes the node, and outputs the results.
 * 
 */
public class Command {
	/** output DOM Document tree */
	protected Document results;

	/** root node of results document */
	protected Element resultsNode;

	/**
	 * stores created cities sorted by their names (used with listCities command)
	 */
	protected final TreeMap<String, City> citiesByName = new TreeMap<String, City>(new Comparator<String>() {

		@Override
		public int compare(String o1, String o2) {
			return o2.compareTo(o1);
		}

	});
	
	protected QuadTree q;

	/**
	 * stores created cities sorted by their locations (used with listCities command)
	 * 
	 *
	 */
	protected final TreeSet<City> citiesByLocation = new TreeSet<City>(
			new CityLocationComparator());

	/**
	 * stores all cities ever mapped in an TreeMap sorted by their name (new cities with same name overwrite old ones)
	 */
	
	protected final TreeMap<City, Integer> allMappedCitiesByName = new TreeMap<City, Integer>(new Comparator<City>() {

		@Override
		public int compare(City o1, City o2) {
			return o2.getName().compareTo(o1.getName());
		}
	});

	/** spatial width and height of the MX Quadtree */
	protected int spatialWidth, spatialHeight;
	
	public CanvasPlus canvas;

	/**
	 * Set the DOM Document tree to send the of processed commands to.
	 * 
	 * Creates the root results node.
	 * 
	 * @param results
	 *            DOM Document tree
	 */
	public void setResults(Document results) {
		this.results = results;
		resultsNode = results.createElement("results");
		results.appendChild(resultsNode);
	}

	/**
	 * Creates a command result element. Initializes the command name.
	 * 
	 * @param node
	 *            the command node to be processed
	 * @return the results node for the command
	 */
	private Element getCommandNode(final Element node) {
		final Element commandNode = results.createElement("command");
		commandNode.setAttribute("name", node.getNodeName());
		return commandNode;
	}

	/**
	 * Processes an integer attribute for a command. Appends the parameter to
	 * the parameters node of the results. Should not throw a number format
	 * exception if the attribute has been defined to be an integer in the
	 * schema and the XML has been validated beforehand.
	 * 
	 * @param commandNode
	 *            node containing information about the command
	 * @param attributeName
	 *            integer attribute to be processed
	 * @param parametersNode
	 *            node to append parameter information to
	 * @return integer attribute value
	 */
	private int processIntegerAttribute(final Element commandNode,
			final String attributeName, final Element parametersNode) {
		final String value = commandNode.getAttribute(attributeName);

		if (parametersNode != null) {
			/* add the parameters to results */
			final Element attributeNode = results.createElement(attributeName);
			attributeNode.setAttribute("value", value);
			parametersNode.appendChild(attributeNode);
		}

		/* return the integer value */
		return Integer.parseInt(value);
	}

	/**
	 * Processes a string attribute for a command. Appends the parameter to the
	 * parameters node of the results.
	 * 
	 * @param commandNode
	 *            node containing information about the command
	 * @param attributeName
	 *            string attribute to be processed
	 * @param parametersNode
	 *            node to append parameter information to
	 * @return string attribute value
	 */
	private String processStringAttribute(final Element commandNode,
			final String attributeName, final Element parametersNode) {
		final String value = commandNode.getAttribute(attributeName);

		if (parametersNode != null) {
			/* add parameters to results */
			final Element attributeNode = results.createElement(attributeName);
			attributeNode.setAttribute("value", value);
			parametersNode.appendChild(attributeNode);
		}

		/* return the string value */
		return value;
	}

	/**
	 * Reports that the requested command could not be performed because of an
	 * error. Appends information about the error to the results.
	 * 
	 * @param type
	 *            type of error that occurred
	 * @param command
	 *            command node being processed
	 * @param parameters
	 *            parameters of command
	 */
	private void addErrorNode(final String type, final Element command,
			final Element parameters) {
		final Element error = results.createElement("error");
		error.setAttribute("type", type);
		error.appendChild(command);
		error.appendChild(parameters);
		resultsNode.appendChild(error);
	}

	/**
	 * Reports that a command was successfully performed. Appends the report to
	 * the results.
	 * 
	 * @param command
	 *            command not being processed
	 * @param parameters
	 *            parameters used by the command
	 * @param output
	 *            any details to be reported about the command processed
	 */
	private void addSuccessNode(final Element command,
			final Element parameters, final Element output) {
		final Element success = results.createElement("success");
		success.appendChild(command);
		success.appendChild(parameters);
		success.appendChild(output);
		resultsNode.appendChild(success);
	}

	/**
	 * Processes the commands node (root of all commands). Gets the spatial
	 * width and height of the map and send the data to the appropriate data
	 * structures.
	 * 
	 * @param node
	 *            commands node to be processed
	 */
	public void processCommands(final Element node) {
		spatialWidth = Integer.parseInt(node.getAttribute("spatialWidth"));
		spatialHeight = Integer.parseInt(node.getAttribute("spatialHeight"));
		
		canvas = new CanvasPlus("MeeshQuest");
		canvas.setFrameSize(spatialWidth, spatialHeight);
		canvas.addRectangle(0, 0, spatialWidth, spatialHeight, Color.WHITE, true);
		canvas.addRectangle(0, 0, spatialWidth, spatialHeight, Color.BLACK, false);
		q = new QuadTree(spatialWidth , spatialHeight);
	}

	/**
	 * Processes a createCity command. Creates a city in the dictionary (Note:
	 * does not map the city). An error occurs if a city with that name or
	 * location is already in the dictionary.
	 * 
	 * @param node
	 *            createCity node to be processed
	 */
	
	public void processMapCity(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		final String name = processStringAttribute(node, "name", parametersNode);
		
		City bmore = citiesByName.get(name);
		
		if(bmore == null) {
			addErrorNode("nameNotInDictionary", commandNode,
					parametersNode);
		} else if(q.find(bmore)) {
			addErrorNode("cityAlreadyMapped", commandNode,
					parametersNode);
		} else if(!q.validCoordinate(bmore)) {
			addErrorNode("cityOutOfBounds", commandNode,
					parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			
			canvas.addPoint(bmore.getName(), bmore.getX(), bmore.getY(), Color.BLACK);
			q.insert(bmore);
			
			addSuccessNode(commandNode ,
					parametersNode , outputNode);
		}
	}
	
	public void processUnmapCity(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		final String name = processStringAttribute(node, "name", parametersNode);
		
		City bmore = citiesByName.get(name);
		
		if(bmore == null) {
			addErrorNode("nameNotInDictionary", commandNode,
					parametersNode);
		} else if(!q.find(bmore)) {
			addErrorNode("cityNotMapped", commandNode,
					parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			canvas.removePoint(bmore.getName(), bmore.getX(), bmore.getY(), Color.BLACK);
			q.remove(bmore);
			addSuccessNode(commandNode ,
					parametersNode , outputNode);
		}
	}
	
	public void processDeleteCity(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		final String name = processStringAttribute(node, "name", parametersNode);
		
		City c = citiesByName.get(name);
		
		if(c == null) {
			addErrorNode("cityDoesNotExist" , commandNode,
					parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			if(q.find(c)) {
				addCityNode(outputNode, "cityUnmapped", c  );
				q.remove(c);
				
			}
			citiesByLocation.remove(c);
			citiesByName.remove(name);
			addSuccessNode(commandNode,
					parametersNode , outputNode);
		}
	}
	
	
	public void processCreateCity(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");

		final String name = processStringAttribute(node, "name", parametersNode);
		final int x = processIntegerAttribute(node, "x", parametersNode);
		final int y = processIntegerAttribute(node, "y", parametersNode);
		final int radius = processIntegerAttribute(node, "radius",
				parametersNode);
		final String color = processStringAttribute(node, "color",
				parametersNode);

		/* create the city */
		final City city = new City(name, x, y, radius, color);

		if (citiesByLocation.contains(city)) {
			addErrorNode("duplicateCityCoordinates", commandNode,
					parametersNode);
		} else if (citiesByName.containsKey(name)) {
			addErrorNode("duplicateCityName", commandNode, parametersNode);
		} else {
			final Element outputNode = results.createElement("output");

			/* add city to dictionary */
			citiesByName.put(name, city);
			citiesByLocation.add(city);

			/* add success node to results */
			addSuccessNode(commandNode, parametersNode, outputNode);
		}
	}

	/**
	 * Clears all the data structures do there are not cities or roads in
	 * existence in the dictionary or on the map.
	 * 
	 * @param node
	 *            clearAll node to be processed
	 */
	public void processClearAll(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		final Element outputNode = results.createElement("output");

		/* clear data structures */
		q.clear();
		citiesByName.clear();
		citiesByLocation.clear();

		/* add success node to results */
		addSuccessNode(commandNode, parametersNode, outputNode);
	}

	/**
	 * Lists all the cities, either by name or by location.
	 * 
	 * @param node
	 *            listCities node to be processed
	 */
	public void processListCities(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		final String sortBy = processStringAttribute(node, "sortBy",
				parametersNode);

		if (citiesByName.isEmpty()) {
			addErrorNode("noCitiesToList", commandNode, parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			final Element cityListNode = results.createElement("cityList");

			Collection<City> cityCollection = null;
			if (sortBy.equals("name")) {
				cityCollection = citiesByName.values();
			} else if (sortBy.equals("coordinate")) {
				cityCollection = citiesByLocation;
			} else {
				/* XML validator failed */
				System.exit(-1);
			}

			for (City c : cityCollection) {
				addCityNode(cityListNode, c);
			}
			outputNode.appendChild(cityListNode);

			/* add success node to results */
			addSuccessNode(commandNode, parametersNode, outputNode);
		}
	}
	
	public void processRangeCities(final Element node) throws IOException {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		final int x = processIntegerAttribute(node, "x",
				parametersNode);
		final int y = processIntegerAttribute(node, "y",
				parametersNode);
		final int radius = processIntegerAttribute(node, "radius",
				parametersNode);
		final String name;
		
		ArrayList<String> s = new ArrayList<String>();
		q.rangeCities(q.root, x, y, radius, s);
		
		if(node.getAttribute("saveMap").compareTo("") != 0) {
			name = processStringAttribute(node , "saveMap" , parametersNode);
			canvas.addCircle(x, y, radius, Color.BLUE, false);
			addLines(q.root , q.xMax+1 , q.xMin , q.yMax+1 , q.yMin);

			canvas.save(name);
		}
		
		if(s.size() == 0) {
			addErrorNode("noCitiesExistInRange", commandNode, parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			final Element cityListNode = results.createElement("cityList");
			
			for(String str:s) {
				addCityNode(cityListNode, citiesByName.get(str));
			}
			//System.out.println(node.getAttribute("saveMap").compareTo("") != 0);
			
			
			outputNode.appendChild(cityListNode);
			
			addSuccessNode(commandNode,
					parametersNode, outputNode);
		}		
	}
	

	public void processPrintPRQuadTree(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		if(q.isEmpty()) {
			addErrorNode("mapIsEmpty" ,
					commandNode , parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			outputNode.appendChild(processQuadTree(q));
			addSuccessNode(commandNode,
					parametersNode , outputNode);
		}
	}
	
	private Element processNode(Node n , int xMax , int xMin , int yMax , int yMin) {
		if(n.isFlyweight()) {
			return processFlyweight(n);
		} else if (n.isLeaf()) {
			return processLeaf(n);
		} else {
			return processInternal(n , xMax , xMin , yMax ,yMin);
		}
	}
	
	private Element processInternal(Node n , int xMax , int xMin , int yMax , int yMin) {
		Element gray = results.createElement("gray");
		gray.setAttribute("y", Integer.toString((yMax+yMin)/2));
		gray.setAttribute("x" , Integer.toString((xMax+xMin)/2));
		
		
		gray.appendChild(processNode(n.nw , 
				(xMax+xMin)/2 , xMin , yMax , yMax/2));
		gray.appendChild(processNode(n.ne , 
				xMax , xMax/2 , yMax , yMax/2));
		gray.appendChild(processNode(n.sw,
				(xMax+xMin)/2 , xMin , (yMax+yMin)/2 , yMin));
		gray.appendChild(processNode(n.se,
				xMax , xMax/2 , (yMax+yMin)/2 , yMin));
		
		
		
		return gray;
	}

	private Element processLeaf(Node n) {
		Element black = results.createElement("black");
		City c = n.ele;
		black.setAttribute("name", c.getName());
		black.setAttribute("x", Integer.toString(c.getX()));
		black.setAttribute("y", Integer.toString(c.getY()));
		return black;
	}

	private Element processFlyweight(Node n) {
		return results.createElement("white");
	}
	
	private Element processQuadTree(QuadTree qt) {
		Element t = results.createElement("quadtree");
		t.appendChild(processNode(qt.root , 
				(int) qt.xMax , (int) qt.xMin , (int) qt.yMax , (int) qt.yMin));
		return t;
	}

	/**
	 * Creates a city node containing information about a city. Appends the city
	 * node to the passed in node.
	 * 
	 * @param node
	 *            node which the city node will be appended to
	 * @param cityNodeName
	 *            name of city node
	 * @param city
	 *            city which the city node will describe
	 */
	
	private void addCityNode(final Element node, final String cityNodeName,
			final City city) {
		final Element cityNode = results.createElement(cityNodeName);
		cityNode.setAttribute("name", city.getName());
		cityNode.setAttribute("x", Integer.toString((int) city.getX()));
		cityNode.setAttribute("y", Integer.toString((int) city.getY()));
		cityNode.setAttribute("radius", Integer
				.toString((int) city.getRadius()));
		cityNode.setAttribute("color", city.getColor());
		node.appendChild(cityNode);
	}

	/**
	 * Creates a city node containing information about a city. Appends the city
	 * node to the passed in node.
	 * 
	 * @param node
	 *            node which the city node will be appended to
	 * @param city
	 *            city which the city node will describe
	 */
	private void addCityNode(final Element node, final City city) {
		addCityNode(node, "city", city);
	}
	
	public void processNearestCity(final Element node) {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		
		final int x = processIntegerAttribute(node, "x", parametersNode);
		final int y = processIntegerAttribute(node, "y", parametersNode);
		
		if(q.isEmpty()) {
			addErrorNode("mapIsEmpty", commandNode,
					parametersNode);
		} else {
			final Element outputNode = results.createElement("output");
			City c = q.nearestCity(x, y, q.root);
			addCityNode(outputNode , c);
			addSuccessNode(commandNode,
					parametersNode , outputNode);
		}
		
	}
	
	public void addLines(Node r , double xMax , double xMin , double yMax , double yMin) {
		if(r.isInternal()) {
			canvas.addLine(xMax/2, yMax, xMax/2, yMin, Color.BLACK);
			canvas.addLine(xMax, yMax/2, xMin, yMax/2, Color.BLACK);
			addLines(r.nw , (xMax+xMin)/2 , xMin , yMax , yMax/2);
			addLines(r.ne , xMax , xMax/2 , yMax , yMax/2);
			addLines(r.sw , (xMax+xMin)/2 , xMin , (yMax+yMin)/2 , yMin);
			addLines(r.se , xMax , xMax/2 , (yMax+yMin)/2 , yMin );
		}
	}

	public void saveMap(Element node) throws IOException {
		final Element commandNode = getCommandNode(node);
		final Element parametersNode = results.createElement("parameters");
		final Element outputNode = results.createElement("output");
		
		final String name = processStringAttribute(node, "name", parametersNode);

		addLines(q.root , q.xMax+1 , q.xMin , q.yMax+1 , q.yMin);

		canvas.save(name);

		addSuccessNode(commandNode, parametersNode, outputNode);
	}


}
