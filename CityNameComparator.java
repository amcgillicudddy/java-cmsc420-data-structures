package cmsc420.structure ;


import java.util.Comparator;

/**
 * Compares two cities based on their names.
 */
public class CityNameComparator implements Comparator<City> {
	public int compare(final City c1, final City c2) {
		return c2.getName().compareTo(c1.getName());
	}
}